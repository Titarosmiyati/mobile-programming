//looping pertama
var text = 2;
while(text <= 20) { 
    if(text % 2 == 0) {
        console.log(text +' - I Love Coding'); 
    }
text++; 
}

//looping kedua
var text = 20;
while(text >= 2) { 
    if(text % 2 == 0) {
        console.log(text +' - I Love Coding'); 
    }
text--; 
}

//looping for
for(i = 1 ; i<=20;i++){
var kondisi = "";  
if(i % 3 == 0 && i % 2 != 0){
kondisi = "I Love Coding";
}else if(i % 2 != 0){
kondisi = "Teknik"
} else if(i % 2 == 0){
kondisi = "Informatika";
}
console.log(i + " - " + kondisi ); 
}

//membuat persegi panjang
var s = '';
var panjang = 8;
var lebar = 4;
for (var i =0; i <=8; i++) {
for (var j =0; j <=4; j++) { 
s += '#';
}
s += '\n';
}
console.log(s);

//Papan Catur
var s = '';
var panjang = 8;
var lebar = 8;
for (var i =0; i <=8; i++) {
if(i % 2 == 0){
for(var j = 0; j <=8; j++){
if (j % 2 == 0 ){
s += '#';
}else {
s += ' '; 
}
}
} else {
for (var j =1; j <=panjang; j++) {
if(j % 2 == 0){ 
s += '#';
}else {
s += ' '; 
}
}
}
s += '\n';
}

console.log(s);

//membuat tangga
var s = '';
var panjang = 7;
var lebar = 7;
for (var i =0; i <=7; i++) {
for (var j =0; j <=i; j++) { 
s += '#';
}
s += '\n';
}
console.log(s);